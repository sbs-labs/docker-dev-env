FROM ubuntu:19.10 as dev-base

ARG HOME_DIR
ARG WORK_DIR
ARG ENV_FILE

ENV HOME_DIR=$HOME_DIR
ENV WORK_DIR=$WORK_DIR
ENV ENV_FILE=$ENV_FILE

WORKDIR ${WORK_DIR}

# Docker prep
# docker is heavy, trying to leverage caching, so it stays at the top
RUN apt update \
  && apt upgrade -y \
  && apt install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common \
  && mkdir /etc/docker-dev

# Install Docker
RUN curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add - \
  && add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu disco stable" \
  && apt update \
  && apt install -y docker-ce docker-ce-cli containerd.io

COPY entrypoint /usr/local/bin/

# update entrypoint with ENV_FILE, ensure path exists
# RUN sed -i 's/{ENV_FILE}/$ENV_FILE/g' "$ENV_FILE" \
#   && echo ${ENV_FILE} | rev | cut -d/ -f2- | rev | xargs mkdir -p

ENTRYPOINT ["/usr/local/bin/entrypoint"]

###############################################################################################
#
# Dev local - specific to machine?
# move this to user profile?
#
###############################################################################################
FROM dev-base as dev-local

ARG USER_ID
ARG USERNAME
ARG HOME_DIR
ARG DOCKER_GID

# Install desired packages
RUN apt install -y \
    zsh \
    git \
    tmux \
    vim

RUN curl -L "https://github.com/docker/compose/releases/download/1.25.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose \
  && chmod +x /usr/local/bin/docker-compose

# add user/group - give access to docker
RUN adduser \
     --uid $USER_ID \
     --gecos "" \
     --disabled-password \
     --disabled-login \
     --home ${HOME_DIR} \
     $USERNAME \
   && chown -R $USERNAME:$USERNAME $HOME_DIR \
   && adduser $USERNAME docker \
   && sed -i -e "s/docker:x:[0-9]\{3,\}/docker:x:$DOCKER_GID/" /etc/group

# Change default shell
RUN chsh -s $(which zsh) $USERNAME

USER $USERNAME

RUN sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" \
  && git clone https://github.com/zsh-users/zsh-autosuggestions ~/.oh-my-zsh/custom/plugins/zsh-autosuggestions

COPY dotfiles/.* "$HOME_DIR/"
