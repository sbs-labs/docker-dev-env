set nocompatible
filetype on
scriptencoding utf-8
set encoding=utf-8

set rtp+=~/.vim/bundle/Vundle.vim
" whitespace
set list
set listchars=tab:»·,trail:~,extends:>,precedes:<

" Syntax highlighting
syntax on

" Line Numbers
set number
set relativenumber " or rnu

" Use spaces insted of tab characters
set expandtab

" tab length is only 2 spaces
set tabstop=2
set shiftwidth=2
set smartindent
set autoindent

" key maps
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-h> <C-w>h
nnoremap <C-l> <C-w>l
map <M-s> :w<kEnter>
imap <M-s> <Esc>:w<kEnter>i
" CTRL+ <- (prev tab)
nnoremap <C-Left> :tabprevious<CR>
" CTRL + -> (next tab)
nnoremap <C-Right> :tabnext<CR>
" ALT + <- (move tab left)
nnoremap <silent> <A-Left> :execute 'silent! tabmove ' . (tabpagenr()-2)<CR>
" ALT + -> (move tab right)
nnoremap <silent> <A-Right> :execute 'silent! tabmove ' . tabpagenr()<CR>
nmap <leader>l :set list!<CR>

" Override Default backspace options
set backspace=indent,eol,start

