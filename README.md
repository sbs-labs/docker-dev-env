# Build and run dev container

to use: create symbolic link in a `PATH` directory

```sh
ln -s <absolute_project_path>/dev-up <PATH_directory>/dev-up
# ex:
# ln -s /home/me/project/docker-dev-env/dev-up /usr/local/bin/dev-up
```


## Build

```sh
./dev-up --build
```

### Build Options

You can specify options to be used during the dev build process as follows:

```sh
[OPTIONS] ./dev-up --build
# ex:
# DEV_BUILD_IMAGE_NAME=new-dev-image ./dev-up --build
```

#### Loading entrypoint scripts

You can add custom `ENTRYPOINT` scripts by copying scripts into the `/entrypoint` directory. all files in the directory will be run, they do not require the `exec $@` to follow - as the primary entryscript will handle that

```sh
COPY --chown=$USERNAME custom_startup_script /entrypoint/
```

#### Option definitions

```sh
# if outside of the project build directory folder, you can still run the build w/ this variable pointing to this project location
# (default: $(pwd) )
DEV_BUILD_DIR=<directory_pa

# use a custom build image name
# (default: dev-image)
DEV_BUILD_IMAGE_NAME=<custom_name>
```

## Progress

contemplating GO cli using this project: https://github.com/urfave/cli/blob/master/docs/v2/manual.md

## TODO:

- [x] git configuration to be inserted into container
- [x] ssh key import
- [ ] gpg key import
- [ ] kubeconfig &/or `$KUBECONFIG`
- [ ] Configurations - using yaml
  - [ ] volumes (to be mounting to docker container)
  - [ ] entrypoint (override or use new) - may not need this with entrypoint copy
  - [ ] dockerfile (file contents or location)
- [ ] simplified package installs - example: kubectl, doctl, helm ... apps not available via apt
  - [ ] list packages to install (remove from docker image - possibly remove personal docker image)
- [ ] default to background?
- [ ] sub commands
  - [ ] `connect` to docker container in another window - `docker exec -it`
- [ ] build off user dockerfile - static at first `~/Dockerfile.dev`
